import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Filter from '../../components/Filter'

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));

const Home = () => {
  const classes = useStyles();
  const [ services, setServices ] = useState([])
  return (
   <Grid item xs={12}>
     <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          Tengo Delivery
        </Typography>
        <Button component="a" href='/nuevo' color="inherit">Crear nuevo</Button>
      </Toolbar>
    </AppBar>
    <br />
    <Filter setServices={list => setServices(list)}/>
    <Divider />
    <br />
    {services.length ? services.map(service => (
      <Grid item xs={4} key={service.title}>
        <Card variant="outlined">
          <CardContent>
            <Typography variant="h5" component="h2">
              {service.title}
            </Typography>
            <Typography color="textSecondary">
              {service.url}
            </Typography>
            <Typography variant="body2" component="p">
              {service.description}
            </Typography>
          </CardContent>
          <CardActions>
            <Button variant="outlined" color="primary" component="a" size="small" href={service.url} target="_blank">Ir al servicio</Button>
          </CardActions>
        </Card>
      </Grid>
    )): <i style={{ display: 'flex', justifyContent: 'center', color: 'gray', textAlign: 'center' }}>
        Intenta buscando servicios por región y comuna,
        <br />sino encuentras y conoces un negocio, invítalo para que se sume</i>}
  </Grid>
  )
}

export default Home