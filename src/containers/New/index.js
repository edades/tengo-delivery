import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import ListSubheader from '@material-ui/core/ListSubheader';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import regions from '../../regions';
import { categoriesRef, servicesRef } from '../../firebase'

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '30ch',  
    },
    display: 'flex',
    flexDirection: 'column',
    width: '350px',
    margin: '0 auto',
    justifyContent: 'center',
    alignItems: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  title: {
    flexGrow: 1,
  },
}));

class NewContainer extends React.Component {
  state = {
    title: '',
    url: '',
    description: '',
    regions: [],
    communes: [],
    categories: [],
    selectedRegions: [],
    selectedCommunes: [],
    selectedCategories: [],
    formError: []
  }

  componentDidMount() {
    this.setState({ regions })
    this.setCategories()
  }

  setCategories = () => {
    categoriesRef.once('value').then(snap => {
      const categories = Object.values(snap.val()).filter(item => item.active).map(item => item.name)
      this.setState({ categories })
    })
  }

  handleCategory = e => this.setState({ selectedCategories: e.target.value })

  getCategories = (classes) => {
    const { categories, selectedCategories } = this.state
    return (<FormControl className={classes.formControl}>
      <InputLabel htmlFor="grouped-select">Categorias</InputLabel>
      <Select
        labelId="demo-mutiple-checkbox-label3"
        id="demo-mutiple-checkbox3"
        multiple
        disabled={!categories.length}
        value={selectedCategories}
        onChange={this.handleCategory}
        input={<Input id="grouped-select" />}
        renderValue={selected => selected.join(', ')}
        MenuProps={MenuProps}>
        {categories.map(cate => (
          <MenuItem key={cate} value={cate}>
            <Checkbox checked={selectedCategories.includes(cate)} />
            <ListItemText primary={cate} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>)
  }

  setCommunes = () => {
    const { selectedRegions } = this.state
    const communes = regions.filter(({ region }) => selectedRegions.includes(region))
    this.setState({ communes })
  }

  handleCommune = e => this.setState({ selectedCommunes: e.target.value })

  getCommunes = (classes) => {
    const { communes, selectedCommunes } = this.state
    if (!communes.length) return null
    return (<FormControl className={classes.formControl}>
      <InputLabel htmlFor="grouped-select">Comunas</InputLabel>
      <Select
        labelId="demo-mutiple-checkbox-label2"
        id="demo-mutiple-checkbox2"
        multiple
        value={selectedCommunes}
        onChange={this.handleCommune}
        input={<Input id="grouped-select" />}
        renderValue={selected => selected.join(', ')}
        MenuProps={MenuProps}>
        {communes.map(item => [
          (<ListSubheader>{item.region}</ListSubheader>),
          item.comunas.map(commune => (
            <MenuItem key={commune} value={commune}>
              <Checkbox checked={selectedCommunes.includes(commune)} />
              <ListItemText primary={commune} />
            </MenuItem>
          ))
        ])}
      </Select>
    </FormControl>)
  }

  handleRegion = async (e) => {
    await this.setState({ selectedRegions: e.target.value });
    if (!e.target.value.includes('Todo Chile')) {
      this.setCommunes();
    } else {
      this.setState({ communes: [] })
    }
  }

  getRegions = (classes) => {
    const { regions, selectedRegions } = this.state
    
    return (<FormControl className={classes.formControl}>
      <InputLabel id="demo-mutiple-checkbox-label">Regiones</InputLabel>
      <Select
        labelId="demo-mutiple-checkbox-label"
        id="demo-mutiple-checkbox"
        multiple
        value={selectedRegions}
        onChange={this.handleRegion}
        input={<Input />}
        renderValue={selected => selected.join(', ')}
        MenuProps={MenuProps}
      >
        {regions.map(({ region }) => (
          <MenuItem key={region} value={region}>
            <Checkbox checked={selectedRegions.includes(region)} />
            <ListItemText primary={region} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>)
  }

  saveService = () => {
    const {
      title,
      url,
      description,
      selectedRegions,
      selectedCommunes,
      selectedCategories
    } = this.state
    let error = []
    if (!this.state.selectedCategories.length) {
      error = [...error, 'selecciona una categoria']
    }
    if (!this.state.selectedRegions.length) {
      error = [...error, 'selecciona una región']
    }
    if (this.state.title === '') {
      error = [...error, 'ingresa un título']
    }
    if (this.state.url === '') {
      error = [...error, 'ingresa una url']
    }
    if (this.state.url === '') {
      error = [...error, 'ingresa una url']
    }
    if (this.state.description === '') {
      error = [...error, 'ingresa una descripción']
    }
    this.setState({ formError: error })
    if (!error.length) {
      servicesRef.push({
        title,
        url,
        description,
        regions: selectedRegions,
        communes: selectedCommunes,
        categories: selectedCategories
      });
      this.setState({
        title: '',
        url: '',
        description: '',
        communes: [],
        categories: [],
        selectedRegions: [],
        selectedCommunes: [],
        selectedCategories: [],
        formError: []
      })
      alert('Servicio creado con éxito, revisalo en la página de inicio')
    }
  }

  Form = () => {
    const classes = useStyles();
    return (
      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          id="title"
          label="Titulo (nombre del local)"
          value={this.state.title}
          onChange={e => this.setState({ title: e.target.value })}/>
        <br />
        <TextField
          id="url"
          label="URL (página web o url de la red social)"
          value={this.state.url}
          onChange={e => this.setState({ url: e.target.value })}  />
        <br />
        {this.getRegions(classes)}
        {this.getCommunes(classes)}
        <br />
        {this.getCategories(classes)}
        <br />
        <TextField
          id="outlined-multiline-static"
          label="Info rápida del servicio"
          multiline
          rows="4"
          placeholder="Ingresa una descripción como tipo de productos, telefono, forma de pago, etc"
          variant="outlined"
          value={this.state.description}
          onChange={e => this.setState({ description: e.target.value })}
        />
        <br />
        <i style={{ color: 'gray', width: '100%', textAlign: 'center' }}>* Todos los campos son obligatorios *<br />&nbsp;&nbsp;excepto la comuna (aparece al seleccionar una región)</i>
        <br />
        <Button
          variant="contained"
          color="primary"
          onClick={this.saveService}
          disabled={
            !this.state.selectedCategories.length
            || !this.state.selectedRegions.length
            || this.state.title === ''
            || this.state.url === ''
            || this.state.description === ''}>
          Guardar
        </Button>
        <div>
          {this.state.formError.map(error => (
            <i>{error}</i>
          ))}
        </div>
      </form>
    )
  }

  Header = () => {
    const classes = useStyles();
    return (
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Tengo Delivery - Crear nuevo servicio
          </Typography>
          <Button component="a" href='/' color="inherit">Volver al inicio</Button>
        </Toolbar>
      </AppBar>
    )
  }

  render () {
    return (
      <>
      <this.Header />
      <br />
      <this.Form />
      </>
    )
  }
}

export default NewContainer