import React from 'react';
import Container from '@material-ui/core/Container';
import Home from './containers/Home'
import NewContainer from './containers/New'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Container> 
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/nuevo">
            <NewContainer />
          </Route>
          <Route path="*">
            <marquee behavior="scroll" direction="down">
              <br /><br /><br />
              Page not found
            </marquee>
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
