import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import regions from '../../regions';
import { categoriesRef, servicesRef } from '../../firebase'

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  }
}));

class Filter extends React.Component {
  state = {
    selectedRegion: '',
    selectedCommune: '',
    selectedCategory: '',
    regions: [],
    communes: [],
    categories: [],
    services: [],
    filteredServices: []
  }

  componentDidMount() {
    this.setState({ regions })
    this.getCategories()
    this.getServices()
  }

  getCategories = () => {
    categoriesRef.once('value').then(snap => {
      const categories = Object.values(snap.val()).filter(item => item.active).map(item => item.name)
      this.setState({ categories })
    })
  }

  getServices = () =>
    servicesRef.once('value').then(snap => this.setState({ services: Object.values(snap.val()) }))

  filterServices = () => {
    const { selectedRegion, selectedCommune, selectedCategory, services } = this.state
    const filteredServices = services.filter(service => {
      let valid = 0
      if (selectedRegion && service.regions.includes(selectedRegion)) {
        valid += 1
      }
      if (selectedCommune && service.communes.includes(selectedCommune)) {
        valid += 1
      }
      if (selectedCategory && service.categories.includes(selectedCategory)) {
        valid += 1
      }
      return service.communes.length ? valid === 3 : valid === 2
    })
    this.setState({ filteredServices }, () => this.props.setServices(this.state.filteredServices))
  }

  Dropdown = ({ title, items, selected }) => {
    const classes = useStyles();
    return (
      <FormControl className={classes.formControl} disabled={items.length === 0}>
        <InputLabel id="demo-simple-select-label">{title}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={this.state[selected]}
          onChange={e => this.handleChange(e, selected)}
        >
          {items.map(item => {
              const value = selected === 'selectedRegion' ? item.region : item
              return (<MenuItem value={value} key={value}>{value}</MenuItem>)
            }
          )}
        </Select>
      </FormControl>
    )
  }

  handleChange = (e, selected) => {
    this.setState({ [selected]: e.target.value })
    this.props.setServices([])
    if (selected === 'selectedRegion' && e.target.value !== 'Todo Chile') {
      const communes = this.state.regions.filter(reg =>
        reg.region === e.target.value)[0].comunas
      this.setState({ communes })
    }
    if (e.target.value === 'Todo Chile') {
      this.setState({ selectedCommune: '', communes: [] })
    }
  }
  render() {
    return (
      <>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <this.Dropdown title="Región" items={this.state.regions} selected="selectedRegion" />
        <this.Dropdown title="Comuna" items={this.state.communes} selected="selectedCommune"/>
        <this.Dropdown title="Categoria" items={this.state.categories} selected="selectedCategory"/>
        &nbsp;&nbsp;&nbsp;
        <Button
          variant="contained"
          color="primary"
          onClick={this.filterServices}
          disabled={this.state.selectedCategory === '' || this.state.selectedRegion === ''}>
          Buscar
        </Button>
      </div>
      <i style={{ display: 'flex', justifyContent: 'center', color: 'gray' }}>Selecciona al menos una región y una categoría para buscar</i>
      <br />
      </>
    )
  }
}

export default Filter